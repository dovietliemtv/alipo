-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2018 at 07:27 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.0.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alipo`
--

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_access_log`
--

INSERT INTO `backend_access_log` (`id`, `user_id`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2018-03-21 09:22:59', '2018-03-21 09:22:59'),
(2, 1, '::1', '2018-03-28 08:26:42', '2018-03-28 08:26:42');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persist_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_activated` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `first_name`, `last_name`, `login`, `email`, `password`, `activation_code`, `persist_code`, `reset_password_code`, `permissions`, `is_activated`, `role_id`, `activated_at`, `last_login`, `created_at`, `updated_at`, `is_superuser`) VALUES
(1, 'Admin', 'Person', 'admin', 'admin@domain.tld', '$2y$10$2s.xg06nx39bCx3.wn1X0.ZNVTqpEZ6BI2mFp9Q1powcjSUUqKBsC', NULL, '$2y$10$fRiJnr3QSHANCdbwLWLY1OMqg1XBcPpYt2cGxTorvnnR3FqOAawWm', NULL, '', 1, 2, NULL, '2018-03-28 08:26:35', '2018-03-21 09:21:07', '2018-03-28 08:26:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_users_groups`
--

INSERT INTO `backend_users_groups` (`user_id`, `user_group_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_groups`
--

INSERT INTO `backend_user_groups` (`id`, `name`, `created_at`, `updated_at`, `code`, `description`, `is_new_user_default`) VALUES
(1, 'Owners', '2018-03-21 09:21:07', '2018-03-21 09:21:07', 'owners', 'Default group for website owners.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

CREATE TABLE `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_roles`
--

INSERT INTO `backend_user_roles` (`id`, `name`, `code`, `description`, `permissions`, `is_system`, `created_at`, `updated_at`) VALUES
(1, 'Publisher', 'publisher', 'Site editor with access to publishing tools.', '', 1, '2018-03-21 09:21:07', '2018-03-21 09:21:07'),
(2, 'Developer', 'developer', 'Site administrator with access to developer tools.', '', 1, '2018-03-21 09:21:07', '2018-03-21 09:21:07');

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `backend_user_throttle`
--

INSERT INTO `backend_user_throttle` (`id`, `user_id`, `ip_address`, `attempts`, `last_attempt_at`, `is_suspended`, `suspended_at`, `is_banned`, `banned_at`) VALUES
(1, 1, '::1', 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `benfreke_menumanager_menus`
--

CREATE TABLE `benfreke_menumanager_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_external` tinyint(1) NOT NULL DEFAULT '0',
  `link_target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `enabled` int(11) NOT NULL DEFAULT '1',
  `parameters` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `query_string` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `benfreke_menumanager_menus`
--

INSERT INTO `benfreke_menumanager_menus` (`id`, `parent_id`, `title`, `description`, `url`, `nest_left`, `nest_right`, `nest_depth`, `created_at`, `updated_at`, `is_external`, `link_target`, `enabled`, `parameters`, `query_string`) VALUES
(1, NULL, 'main menu', '', NULL, 1, 14, 0, '2018-03-28 08:36:27', '2018-03-28 08:39:41', 0, '_self', 1, '', ''),
(2, 1, 'Home', 'Home', 'home', 2, 3, 1, '2018-03-28 08:36:40', '2018-03-28 08:37:46', 0, '_self', 1, '', ''),
(3, 1, 'Works', 'Works', 'works', 4, 5, 1, '2018-03-28 08:36:53', '2018-03-28 08:37:53', 0, '_self', 1, '', ''),
(4, 1, 'Reel', 'Reel', 'reel', 6, 7, 1, '2018-03-28 08:37:11', '2018-03-28 08:37:57', 0, '_self', 1, '', ''),
(5, 1, 'About', 'About', 'about', 8, 9, 1, '2018-03-28 08:37:28', '2018-03-28 08:37:58', 0, '_self', 1, '', ''),
(6, 1, 'Contact', 'Contact', 'contact', 12, 13, 1, '2018-03-28 08:37:41', '2018-03-28 08:39:41', 0, '_self', 1, '', ''),
(7, 1, 'Clients', 'Clients', 'clients', 10, 11, 1, '2018-03-28 08:39:18', '2018-03-28 08:39:41', 0, '_self', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

CREATE TABLE `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_template` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `old_content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slave_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `domdom_cms_abouts`
--

CREATE TABLE `domdom_cms_abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `domdom_cms_category_works`
--

CREATE TABLE `domdom_cms_category_works` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_work_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `domdom_cms_clients`
--

CREATE TABLE `domdom_cms_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `clients` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `domdom_cms_contacts`
--

CREATE TABLE `domdom_cms_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `domdom_cms_ganeral_options`
--

CREATE TABLE `domdom_cms_ganeral_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `social_network` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `domdom_cms_homes`
--

CREATE TABLE `domdom_cms_homes` (
  `id` int(10) UNSIGNED NOT NULL,
  `heading` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `domdom_cms_reels`
--

CREATE TABLE `domdom_cms_reels` (
  `id` int(10) UNSIGNED NOT NULL,
  `vimeo_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `domdom_cms_works`
--

CREATE TABLE `domdom_cms_works` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `vimeo_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduce_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduce_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2013_10_01_000001_Db_Deferred_Bindings', 1),
(2, '2013_10_01_000002_Db_System_Files', 1),
(3, '2013_10_01_000003_Db_System_Plugin_Versions', 1),
(4, '2013_10_01_000004_Db_System_Plugin_History', 1),
(5, '2013_10_01_000005_Db_System_Settings', 1),
(6, '2013_10_01_000006_Db_System_Parameters', 1),
(7, '2013_10_01_000007_Db_System_Add_Disabled_Flag', 1),
(8, '2013_10_01_000008_Db_System_Mail_Templates', 1),
(9, '2013_10_01_000009_Db_System_Mail_Layouts', 1),
(10, '2014_10_01_000010_Db_Jobs', 1),
(11, '2014_10_01_000011_Db_System_Event_Logs', 1),
(12, '2014_10_01_000012_Db_System_Request_Logs', 1),
(13, '2014_10_01_000013_Db_System_Sessions', 1),
(14, '2015_10_01_000014_Db_System_Mail_Layout_Rename', 1),
(15, '2015_10_01_000015_Db_System_Add_Frozen_Flag', 1),
(16, '2015_10_01_000016_Db_Cache', 1),
(17, '2015_10_01_000017_Db_System_Revisions', 1),
(18, '2015_10_01_000018_Db_FailedJobs', 1),
(19, '2016_10_01_000019_Db_System_Plugin_History_Detail_Text', 1),
(20, '2016_10_01_000020_Db_System_Timestamp_Fix', 1),
(21, '2017_08_04_121309_Db_Deferred_Bindings_Add_Index_Session', 1),
(22, '2017_10_01_000021_Db_System_Sessions_Update', 1),
(23, '2017_10_01_000022_Db_Jobs_FailedJobs_Update', 1),
(24, '2017_10_01_000023_Db_System_Mail_Partials', 1),
(25, '2013_10_01_000001_Db_Backend_Users', 2),
(26, '2013_10_01_000002_Db_Backend_User_Groups', 2),
(27, '2013_10_01_000003_Db_Backend_Users_Groups', 2),
(28, '2013_10_01_000004_Db_Backend_User_Throttle', 2),
(29, '2014_01_04_000005_Db_Backend_User_Preferences', 2),
(30, '2014_10_01_000006_Db_Backend_Access_Log', 2),
(31, '2014_10_01_000007_Db_Backend_Add_Description_Field', 2),
(32, '2015_10_01_000008_Db_Backend_Add_Superuser_Flag', 2),
(33, '2016_10_01_000009_Db_Backend_Timestamp_Fix', 2),
(34, '2017_10_01_000010_Db_Backend_User_Roles', 2),
(35, '2014_10_01_000001_Db_Cms_Theme_Data', 3),
(36, '2016_10_01_000002_Db_Cms_Timestamp_Fix', 3),
(37, '2017_10_01_000003_Db_Cms_Theme_Logs', 3);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `details` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_event_logs`
--

INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(1, 'error', 'Exception: Stop everything!!! This file already exists: E:\\xampp\\htdocs\\alipo/plugins/domdom/cms/models/Home.php in E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Scaffold\\GeneratorCommand.php:117\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Scaffold\\GeneratorCommand.php(84): October\\Rain\\Scaffold\\GeneratorCommand->makeStub(\'model/model.stu...\')\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Scaffold\\GeneratorCommand.php(62): October\\Rain\\Scaffold\\GeneratorCommand->makeStubs()\n#2 [internal function]: October\\Rain\\Scaffold\\GeneratorCommand->handle()\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(29): call_user_func_array(Array, Array)\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(87): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(31): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(October\\Rain\\Foundation\\Application), Array, Object(Closure))\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(549): Illuminate\\Container\\BoundMethod::call(Object(October\\Rain\\Foundation\\Application), Array, Array, NULL)\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(183): Illuminate\\Container\\Container->call(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Command\\Command.php(252): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(170): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(946): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(248): Symfony\\Component\\Console\\Application->doRunCommand(Object(October\\Rain\\Scaffold\\Console\\CreateModel), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#12 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#13 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#14 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#15 E:\\xampp\\htdocs\\alipo\\artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#16 {main}', NULL, '2018-03-22 07:57:05', '2018-03-22 07:57:05'),
(2, 'error', 'PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.benfreke_menumanager_menus\' doesn\'t exist in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:77\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php(77): PDO->prepare(\'select * from `...\', Array)\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(326): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'select * from `...\')\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'select * from `...\', Array)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select * from `...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select * from `...\', Array, Object(Closure))\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select * from `...\', Array, true)\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(479): October\\Rain\\Database\\QueryBuilder->get(Array)\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(463): Illuminate\\Database\\Eloquent\\Builder->getModels(Array)\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Concerns\\BuildsQueries.php(77): Illuminate\\Database\\Eloquent\\Builder->get(Array)\n#12 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(289): Illuminate\\Database\\Eloquent\\Builder->first(Array)\n#13 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(1470): Illuminate\\Database\\Eloquent\\Builder->find(\'2\')\n#14 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(393): Illuminate\\Database\\Eloquent\\Model->__call(\'find\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Model.php(428): October\\Rain\\Database\\Model->extendableCall(\'find\', Array)\n#16 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(1482): October\\Rain\\Database\\Model->__call(\'find\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\plugins\\benfreke\\menumanager\\components\\Menu.php(110): Illuminate\\Database\\Eloquent\\Model::__callStatic(\'find\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(1005): BenFreke\\MenuManager\\Components\\Menu->onRender()\n#19 E:\\xampp\\htdocs\\alipo\\modules\\cms\\twig\\Extension.php(128): Cms\\Classes\\Controller->renderComponent(\'menu\', Array)\n#20 E:\\xampp\\htdocs\\alipo\\storage\\cms\\twig\\8f\\8fbc3dd30a1996128715d9244e476ea27a265a83efddb7ae68a54cc4db7f0e19.php(42): Cms\\Twig\\Extension->componentFunction(\'menu\', Array)\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(389): __TwigTemplate_bfa953f8f7da90415132a6298d318a231bdadcef973dd19c6fd4423d57825d7a->doDisplay(Array, Array)\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(366): Twig_Template->displayWithErrorHandling(Array, Array)\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(374): Twig_Template->display(Array)\n#24 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(922): Twig_Template->render(Array)\n#25 E:\\xampp\\htdocs\\alipo\\modules\\cms\\twig\\Extension.php(106): Cms\\Classes\\Controller->renderPartial(\'menu\', Array, true)\n#26 E:\\xampp\\htdocs\\alipo\\storage\\cms\\twig\\42\\423ec139319e931c8ef7ed13ee55147dec3523ae703d574f7e29e2d10e477341.php(44): Cms\\Twig\\Extension->partialFunction(\'header\', Array, true)\n#27 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(389): __TwigTemplate_a2b9540bf01208ab3513d9454ff1e47d9bbf065678ac9aa3c0ffcc8ceeda6d31->doDisplay(Array, Array)\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(366): Twig_Template->displayWithErrorHandling(Array, Array)\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(374): Twig_Template->display(Array)\n#30 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(377): Twig_Template->render(Array)\n#31 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(213): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#32 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#33 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#63 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#64 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#65 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#66 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#67 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#68 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#69 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.benfreke_menumanager_menus\' doesn\'t exist in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:79\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(326): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'select * from `...\')\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'select * from `...\', Array)\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select * from `...\', Array, Object(Closure))\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select * from `...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select * from `...\', Array, true)\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(479): October\\Rain\\Database\\QueryBuilder->get(Array)\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(463): Illuminate\\Database\\Eloquent\\Builder->getModels(Array)\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Concerns\\BuildsQueries.php(77): Illuminate\\Database\\Eloquent\\Builder->get(Array)\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(289): Illuminate\\Database\\Eloquent\\Builder->first(Array)\n#12 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(1470): Illuminate\\Database\\Eloquent\\Builder->find(\'2\')\n#13 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(393): Illuminate\\Database\\Eloquent\\Model->__call(\'find\', Array)\n#14 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Model.php(428): October\\Rain\\Database\\Model->extendableCall(\'find\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(1482): October\\Rain\\Database\\Model->__call(\'find\', Array)\n#16 E:\\xampp\\htdocs\\alipo\\plugins\\benfreke\\menumanager\\components\\Menu.php(110): Illuminate\\Database\\Eloquent\\Model::__callStatic(\'find\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(1005): BenFreke\\MenuManager\\Components\\Menu->onRender()\n#18 E:\\xampp\\htdocs\\alipo\\modules\\cms\\twig\\Extension.php(128): Cms\\Classes\\Controller->renderComponent(\'menu\', Array)\n#19 E:\\xampp\\htdocs\\alipo\\storage\\cms\\twig\\8f\\8fbc3dd30a1996128715d9244e476ea27a265a83efddb7ae68a54cc4db7f0e19.php(42): Cms\\Twig\\Extension->componentFunction(\'menu\', Array)\n#20 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(389): __TwigTemplate_bfa953f8f7da90415132a6298d318a231bdadcef973dd19c6fd4423d57825d7a->doDisplay(Array, Array)\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(366): Twig_Template->displayWithErrorHandling(Array, Array)\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(374): Twig_Template->display(Array)\n#23 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(922): Twig_Template->render(Array)\n#24 E:\\xampp\\htdocs\\alipo\\modules\\cms\\twig\\Extension.php(106): Cms\\Classes\\Controller->renderPartial(\'menu\', Array, true)\n#25 E:\\xampp\\htdocs\\alipo\\storage\\cms\\twig\\42\\423ec139319e931c8ef7ed13ee55147dec3523ae703d574f7e29e2d10e477341.php(44): Cms\\Twig\\Extension->partialFunction(\'header\', Array, true)\n#26 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(389): __TwigTemplate_a2b9540bf01208ab3513d9454ff1e47d9bbf065678ac9aa3c0ffcc8ceeda6d31->doDisplay(Array, Array)\n#27 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(366): Twig_Template->displayWithErrorHandling(Array, Array)\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(374): Twig_Template->display(Array)\n#29 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(377): Twig_Template->render(Array)\n#30 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(213): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#31 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#32 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#63 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#64 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#65 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#66 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#67 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#68 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.benfreke_menumanager_menus\' doesn\'t exist (SQL: select * from `benfreke_menumanager_menus` where `benfreke_menumanager_menus`.`id` = 2 order by `nest_left` asc limit 1) in E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php:664\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select * from `...\', Array, Object(Closure))\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select * from `...\', Array, Object(Closure))\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select * from `...\', Array, true)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(479): October\\Rain\\Database\\QueryBuilder->get(Array)\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(463): Illuminate\\Database\\Eloquent\\Builder->getModels(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Concerns\\BuildsQueries.php(77): Illuminate\\Database\\Eloquent\\Builder->get(Array)\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(289): Illuminate\\Database\\Eloquent\\Builder->first(Array)\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(1470): Illuminate\\Database\\Eloquent\\Builder->find(\'2\')\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(393): Illuminate\\Database\\Eloquent\\Model->__call(\'find\', Array)\n#12 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Model.php(428): October\\Rain\\Database\\Model->extendableCall(\'find\', Array)\n#13 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(1482): October\\Rain\\Database\\Model->__call(\'find\', Array)\n#14 E:\\xampp\\htdocs\\alipo\\plugins\\benfreke\\menumanager\\components\\Menu.php(110): Illuminate\\Database\\Eloquent\\Model::__callStatic(\'find\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(1005): BenFreke\\MenuManager\\Components\\Menu->onRender()\n#16 E:\\xampp\\htdocs\\alipo\\modules\\cms\\twig\\Extension.php(128): Cms\\Classes\\Controller->renderComponent(\'menu\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\storage\\cms\\twig\\8f\\8fbc3dd30a1996128715d9244e476ea27a265a83efddb7ae68a54cc4db7f0e19.php(42): Cms\\Twig\\Extension->componentFunction(\'menu\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(389): __TwigTemplate_bfa953f8f7da90415132a6298d318a231bdadcef973dd19c6fd4423d57825d7a->doDisplay(Array, Array)\n#19 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(366): Twig_Template->displayWithErrorHandling(Array, Array)\n#20 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(374): Twig_Template->display(Array)\n#21 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(922): Twig_Template->render(Array)\n#22 E:\\xampp\\htdocs\\alipo\\modules\\cms\\twig\\Extension.php(106): Cms\\Classes\\Controller->renderPartial(\'menu\', Array, true)\n#23 E:\\xampp\\htdocs\\alipo\\storage\\cms\\twig\\42\\423ec139319e931c8ef7ed13ee55147dec3523ae703d574f7e29e2d10e477341.php(44): Cms\\Twig\\Extension->partialFunction(\'header\', Array, true)\n#24 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(389): __TwigTemplate_a2b9540bf01208ab3513d9454ff1e47d9bbf065678ac9aa3c0ffcc8ceeda6d31->doDisplay(Array, Array)\n#25 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(366): Twig_Template->displayWithErrorHandling(Array, Array)\n#26 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(374): Twig_Template->display(Array)\n#27 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(377): Twig_Template->render(Array)\n#28 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(213): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#29 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#30 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#63 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#64 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#65 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#66 {main}\n\nNext Twig_Error_Runtime: An exception has been thrown during the rendering of a template (\"SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.benfreke_menumanager_menus\' doesn\'t exist (SQL: select * from `benfreke_menumanager_menus` where `benfreke_menumanager_menus`.`id` = 2 order by `nest_left` asc limit 1)\") in \"E:\\xampp\\htdocs\\alipo/themes/alipo/partials/header.htm\" at line 11. in E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php:404\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(366): Twig_Template->displayWithErrorHandling(Array, Array)\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(374): Twig_Template->display(Array)\n#2 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(922): Twig_Template->render(Array)\n#3 E:\\xampp\\htdocs\\alipo\\modules\\cms\\twig\\Extension.php(106): Cms\\Classes\\Controller->renderPartial(\'menu\', Array, true)\n#4 E:\\xampp\\htdocs\\alipo\\storage\\cms\\twig\\42\\423ec139319e931c8ef7ed13ee55147dec3523ae703d574f7e29e2d10e477341.php(44): Cms\\Twig\\Extension->partialFunction(\'header\', Array, true)\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(389): __TwigTemplate_a2b9540bf01208ab3513d9454ff1e47d9bbf065678ac9aa3c0ffcc8ceeda6d31->doDisplay(Array, Array)\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(366): Twig_Template->displayWithErrorHandling(Array, Array)\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\twig\\twig\\lib\\Twig\\Template.php(374): Twig_Template->display(Array)\n#8 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(377): Twig_Template->render(Array)\n#9 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\Controller.php(213): Cms\\Classes\\Controller->runPage(Object(Cms\\Classes\\Page))\n#10 E:\\xampp\\htdocs\\alipo\\modules\\cms\\classes\\CmsController.php(50): Cms\\Classes\\Controller->run(\'/\')\n#11 [internal function]: Cms\\Classes\\CmsController->run(\'/\')\n#12 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#13 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#14 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Cms\\Classes\\CmsController), \'run\')\n#15 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#16 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#17 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#18 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#20 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#24 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#25 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#26 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#27 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#47 {main}', NULL, '2018-03-28 08:25:32', '2018-03-28 08:25:32');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(3, 'error', 'PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.domdom_cms_reels\' doesn\'t exist in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:77\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php(77): PDO->prepare(\'select count(*)...\', Array)\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(326): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'select count(*)...\')\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'select count(*)...\', Array)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select count(*)...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select count(*)...\', Array, Object(Closure))\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select count(*)...\', Array, true)\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1792): October\\Rain\\Database\\QueryBuilder->get()\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1765): Illuminate\\Database\\Query\\Builder->runPaginationCountQuery(Array)\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Builder.php(126): Illuminate\\Database\\Query\\Builder->getCountForPagination()\n#12 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(544): October\\Rain\\Database\\Builder->paginate(20, 1)\n#13 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(252): Backend\\Widgets\\Lists->getRecords()\n#14 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(240): Backend\\Widgets\\Lists->prepareVars()\n#15 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\listcontroller\\partials\\_container.htm(9): Backend\\Widgets\\Lists->render()\n#16 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#17 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\ControllerBehavior.php(143): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(98): Backend\\Classes\\ControllerBehavior->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#19 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(394): Backend\\Classes\\ControllerBehavior->makePartial(\'_container.htm\', Array)\n#20 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(381): Backend\\Behaviors\\ListController->listMakePartial(\'container\', Array)\n#21 [internal function]: Backend\\Behaviors\\ListController->listRender()\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(379): call_user_func_array(Array, Array)\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\Extendable.php(46): October\\Rain\\Extension\\Extendable->extendableCall(\'listRender\', Array)\n#24 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\controllers\\reel\\index.htm(2): October\\Rain\\Extension\\Extendable->__call(\'listRender\', Array)\n#25 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#26 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(110): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\')\n#27 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(365): Backend\\Classes\\Controller->makeView(\'index\')\n#28 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(247): Backend\\Classes\\Controller->execPageAction(\'index\', Array)\n#29 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\BackendController.php(112): Backend\\Classes\\Controller->run(\'index\', Array)\n#30 [internal function]: Backend\\Classes\\BackendController->run(\'domdom/cms/reel\')\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#63 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#64 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#65 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#66 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.domdom_cms_reels\' doesn\'t exist in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:79\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(326): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'select count(*)...\')\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'select count(*)...\', Array)\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select count(*)...\', Array, Object(Closure))\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select count(*)...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select count(*)...\', Array, true)\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1792): October\\Rain\\Database\\QueryBuilder->get()\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1765): Illuminate\\Database\\Query\\Builder->runPaginationCountQuery(Array)\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Builder.php(126): Illuminate\\Database\\Query\\Builder->getCountForPagination()\n#11 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(544): October\\Rain\\Database\\Builder->paginate(20, 1)\n#12 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(252): Backend\\Widgets\\Lists->getRecords()\n#13 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(240): Backend\\Widgets\\Lists->prepareVars()\n#14 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\listcontroller\\partials\\_container.htm(9): Backend\\Widgets\\Lists->render()\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#16 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\ControllerBehavior.php(143): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(98): Backend\\Classes\\ControllerBehavior->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(394): Backend\\Classes\\ControllerBehavior->makePartial(\'_container.htm\', Array)\n#19 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(381): Backend\\Behaviors\\ListController->listMakePartial(\'container\', Array)\n#20 [internal function]: Backend\\Behaviors\\ListController->listRender()\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(379): call_user_func_array(Array, Array)\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\Extendable.php(46): October\\Rain\\Extension\\Extendable->extendableCall(\'listRender\', Array)\n#23 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\controllers\\reel\\index.htm(2): October\\Rain\\Extension\\Extendable->__call(\'listRender\', Array)\n#24 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#25 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(110): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\')\n#26 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(365): Backend\\Classes\\Controller->makeView(\'index\')\n#27 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(247): Backend\\Classes\\Controller->execPageAction(\'index\', Array)\n#28 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\BackendController.php(112): Backend\\Classes\\Controller->run(\'index\', Array)\n#29 [internal function]: Backend\\Classes\\BackendController->run(\'domdom/cms/reel\')\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#63 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#64 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#65 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.domdom_cms_reels\' doesn\'t exist (SQL: select count(*) as aggregate from `domdom_cms_reels`) in E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php:664\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select count(*)...\', Array, Object(Closure))\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select count(*)...\', Array, Object(Closure))\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select count(*)...\', Array, true)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1792): October\\Rain\\Database\\QueryBuilder->get()\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1765): Illuminate\\Database\\Query\\Builder->runPaginationCountQuery(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Builder.php(126): Illuminate\\Database\\Query\\Builder->getCountForPagination()\n#9 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(544): October\\Rain\\Database\\Builder->paginate(20, 1)\n#10 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(252): Backend\\Widgets\\Lists->getRecords()\n#11 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(240): Backend\\Widgets\\Lists->prepareVars()\n#12 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\listcontroller\\partials\\_container.htm(9): Backend\\Widgets\\Lists->render()\n#13 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#14 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\ControllerBehavior.php(143): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(98): Backend\\Classes\\ControllerBehavior->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#16 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(394): Backend\\Classes\\ControllerBehavior->makePartial(\'_container.htm\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(381): Backend\\Behaviors\\ListController->listMakePartial(\'container\', Array)\n#18 [internal function]: Backend\\Behaviors\\ListController->listRender()\n#19 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(379): call_user_func_array(Array, Array)\n#20 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\Extendable.php(46): October\\Rain\\Extension\\Extendable->extendableCall(\'listRender\', Array)\n#21 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\controllers\\reel\\index.htm(2): October\\Rain\\Extension\\Extendable->__call(\'listRender\', Array)\n#22 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#23 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(110): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\')\n#24 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(365): Backend\\Classes\\Controller->makeView(\'index\')\n#25 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(247): Backend\\Classes\\Controller->execPageAction(\'index\', Array)\n#26 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\BackendController.php(112): Backend\\Classes\\Controller->run(\'index\', Array)\n#27 [internal function]: Backend\\Classes\\BackendController->run(\'domdom/cms/reel\')\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#63 {main}', NULL, '2018-03-28 08:26:58', '2018-03-28 08:26:58');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(4, 'error', 'PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.domdom_cms_contacts\' doesn\'t exist in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:77\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php(77): PDO->prepare(\'select count(*)...\', Array)\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(326): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'select count(*)...\')\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'select count(*)...\', Array)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select count(*)...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select count(*)...\', Array, Object(Closure))\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select count(*)...\', Array, true)\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1792): October\\Rain\\Database\\QueryBuilder->get()\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1765): Illuminate\\Database\\Query\\Builder->runPaginationCountQuery(Array)\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Builder.php(126): Illuminate\\Database\\Query\\Builder->getCountForPagination()\n#12 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(544): October\\Rain\\Database\\Builder->paginate(20, 1)\n#13 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(252): Backend\\Widgets\\Lists->getRecords()\n#14 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(240): Backend\\Widgets\\Lists->prepareVars()\n#15 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\listcontroller\\partials\\_container.htm(9): Backend\\Widgets\\Lists->render()\n#16 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#17 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\ControllerBehavior.php(143): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(98): Backend\\Classes\\ControllerBehavior->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#19 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(394): Backend\\Classes\\ControllerBehavior->makePartial(\'_container.htm\', Array)\n#20 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(381): Backend\\Behaviors\\ListController->listMakePartial(\'container\', Array)\n#21 [internal function]: Backend\\Behaviors\\ListController->listRender()\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(379): call_user_func_array(Array, Array)\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\Extendable.php(46): October\\Rain\\Extension\\Extendable->extendableCall(\'listRender\', Array)\n#24 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\controllers\\contact\\index.htm(2): October\\Rain\\Extension\\Extendable->__call(\'listRender\', Array)\n#25 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#26 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(110): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\')\n#27 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(365): Backend\\Classes\\Controller->makeView(\'index\')\n#28 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(247): Backend\\Classes\\Controller->execPageAction(\'index\', Array)\n#29 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\BackendController.php(112): Backend\\Classes\\Controller->run(\'index\', Array)\n#30 [internal function]: Backend\\Classes\\BackendController->run(\'domdom/cms/cont...\')\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#63 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#64 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#65 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#66 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.domdom_cms_contacts\' doesn\'t exist in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:79\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(326): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'select count(*)...\')\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'select count(*)...\', Array)\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select count(*)...\', Array, Object(Closure))\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select count(*)...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select count(*)...\', Array, true)\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1792): October\\Rain\\Database\\QueryBuilder->get()\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1765): Illuminate\\Database\\Query\\Builder->runPaginationCountQuery(Array)\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Builder.php(126): Illuminate\\Database\\Query\\Builder->getCountForPagination()\n#11 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(544): October\\Rain\\Database\\Builder->paginate(20, 1)\n#12 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(252): Backend\\Widgets\\Lists->getRecords()\n#13 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(240): Backend\\Widgets\\Lists->prepareVars()\n#14 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\listcontroller\\partials\\_container.htm(9): Backend\\Widgets\\Lists->render()\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#16 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\ControllerBehavior.php(143): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(98): Backend\\Classes\\ControllerBehavior->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(394): Backend\\Classes\\ControllerBehavior->makePartial(\'_container.htm\', Array)\n#19 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(381): Backend\\Behaviors\\ListController->listMakePartial(\'container\', Array)\n#20 [internal function]: Backend\\Behaviors\\ListController->listRender()\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(379): call_user_func_array(Array, Array)\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\Extendable.php(46): October\\Rain\\Extension\\Extendable->extendableCall(\'listRender\', Array)\n#23 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\controllers\\contact\\index.htm(2): October\\Rain\\Extension\\Extendable->__call(\'listRender\', Array)\n#24 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#25 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(110): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\')\n#26 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(365): Backend\\Classes\\Controller->makeView(\'index\')\n#27 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(247): Backend\\Classes\\Controller->execPageAction(\'index\', Array)\n#28 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\BackendController.php(112): Backend\\Classes\\Controller->run(\'index\', Array)\n#29 [internal function]: Backend\\Classes\\BackendController->run(\'domdom/cms/cont...\')\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#63 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#64 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#65 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.domdom_cms_contacts\' doesn\'t exist (SQL: select count(*) as aggregate from `domdom_cms_contacts`) in E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php:664\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select count(*)...\', Array, Object(Closure))\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select count(*)...\', Array, Object(Closure))\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select count(*)...\', Array, true)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1792): October\\Rain\\Database\\QueryBuilder->get()\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1765): Illuminate\\Database\\Query\\Builder->runPaginationCountQuery(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Builder.php(126): Illuminate\\Database\\Query\\Builder->getCountForPagination()\n#9 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(544): October\\Rain\\Database\\Builder->paginate(20, 1)\n#10 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(252): Backend\\Widgets\\Lists->getRecords()\n#11 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(240): Backend\\Widgets\\Lists->prepareVars()\n#12 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\listcontroller\\partials\\_container.htm(9): Backend\\Widgets\\Lists->render()\n#13 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#14 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\ControllerBehavior.php(143): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(98): Backend\\Classes\\ControllerBehavior->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#16 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(394): Backend\\Classes\\ControllerBehavior->makePartial(\'_container.htm\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(381): Backend\\Behaviors\\ListController->listMakePartial(\'container\', Array)\n#18 [internal function]: Backend\\Behaviors\\ListController->listRender()\n#19 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(379): call_user_func_array(Array, Array)\n#20 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\Extendable.php(46): October\\Rain\\Extension\\Extendable->extendableCall(\'listRender\', Array)\n#21 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\controllers\\contact\\index.htm(2): October\\Rain\\Extension\\Extendable->__call(\'listRender\', Array)\n#22 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#23 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(110): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\')\n#24 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(365): Backend\\Classes\\Controller->makeView(\'index\')\n#25 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(247): Backend\\Classes\\Controller->execPageAction(\'index\', Array)\n#26 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\BackendController.php(112): Backend\\Classes\\Controller->run(\'index\', Array)\n#27 [internal function]: Backend\\Classes\\BackendController->run(\'domdom/cms/cont...\')\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#63 {main}', NULL, '2018-03-28 08:27:04', '2018-03-28 08:27:04');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(5, 'error', 'PDOException: SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near \'json not null, `created_at` timestamp null, `updated_at` timestamp null) default\' at line 1 in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:77\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php(77): PDO->prepare(\'create table `d...\', Array)\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(452): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'create table `d...\')\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'create table `d...\', Array)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'create table `d...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(459): Illuminate\\Database\\Connection->run(\'create table `d...\', Array, Object(Closure))\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Blueprint.php(86): Illuminate\\Database\\Connection->statement(\'create table `d...\')\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(252): Illuminate\\Database\\Schema\\Blueprint->build(Object(October\\Rain\\Database\\Connections\\MySqlConnection), Object(Illuminate\\Database\\Schema\\Grammars\\MySqlGrammar))\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(165): Illuminate\\Database\\Schema\\Builder->build(Object(October\\Rain\\Database\\Schema\\Blueprint))\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\Schema\\Builder->create(\'domdom_cms_clie...\', Object(Closure))\n#9 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\updates\\create_clients_table.php(17): Illuminate\\Support\\Facades\\Facade::__callStatic(\'create\', Array)\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(35): Domdom\\Cms\\Updates\\CreateClientsTable->up()\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Concerns\\ManagesTransactions.php(29): October\\Rain\\Database\\Updater->October\\Rain\\Database\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#12 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#13 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#14 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(40): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(397): October\\Rain\\Database\\Updater->setUp(\'E:/xampp/htdocs...\')\n#16 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(146): System\\Classes\\VersionManager->applyDatabaseScript(\'Domdom.Cms\', \'1.0.6\', \'create_clients_...\')\n#17 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(99): System\\Classes\\VersionManager->applyPluginUpdate(\'Domdom.Cms\', \'1.0.6\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(519): System\\Classes\\VersionManager->updatePlugin(Object(Domdom\\Cms\\Plugin))\n#19 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(155): System\\Classes\\UpdateManager->updatePlugin(\'Domdom.Cms\')\n#20 E:\\xampp\\htdocs\\alipo\\modules\\system\\console\\OctoberUp.php(45): System\\Classes\\UpdateManager->update()\n#21 [internal function]: System\\Console\\OctoberUp->handle()\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(29): call_user_func_array(Array, Array)\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(87): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#24 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(31): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(October\\Rain\\Foundation\\Application), Array, Object(Closure))\n#25 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(549): Illuminate\\Container\\BoundMethod::call(Object(October\\Rain\\Foundation\\Application), Array, Array, NULL)\n#26 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(183): Illuminate\\Container\\Container->call(Array)\n#27 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Command\\Command.php(252): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(170): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(946): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(248): Symfony\\Component\\Console\\Application->doRunCommand(Object(System\\Console\\OctoberUp), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 E:\\xampp\\htdocs\\alipo\\artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#35 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near \'json not null, `created_at` timestamp null, `updated_at` timestamp null) default\' at line 1 in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:79\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(452): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'create table `d...\')\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'create table `d...\', Array)\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'create table `d...\', Array, Object(Closure))\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(459): Illuminate\\Database\\Connection->run(\'create table `d...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Blueprint.php(86): Illuminate\\Database\\Connection->statement(\'create table `d...\')\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(252): Illuminate\\Database\\Schema\\Blueprint->build(Object(October\\Rain\\Database\\Connections\\MySqlConnection), Object(Illuminate\\Database\\Schema\\Grammars\\MySqlGrammar))\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(165): Illuminate\\Database\\Schema\\Builder->build(Object(October\\Rain\\Database\\Schema\\Blueprint))\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\Schema\\Builder->create(\'domdom_cms_clie...\', Object(Closure))\n#8 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\updates\\create_clients_table.php(17): Illuminate\\Support\\Facades\\Facade::__callStatic(\'create\', Array)\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(35): Domdom\\Cms\\Updates\\CreateClientsTable->up()\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Concerns\\ManagesTransactions.php(29): October\\Rain\\Database\\Updater->October\\Rain\\Database\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#12 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#13 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(40): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#14 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(397): October\\Rain\\Database\\Updater->setUp(\'E:/xampp/htdocs...\')\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(146): System\\Classes\\VersionManager->applyDatabaseScript(\'Domdom.Cms\', \'1.0.6\', \'create_clients_...\')\n#16 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(99): System\\Classes\\VersionManager->applyPluginUpdate(\'Domdom.Cms\', \'1.0.6\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(519): System\\Classes\\VersionManager->updatePlugin(Object(Domdom\\Cms\\Plugin))\n#18 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(155): System\\Classes\\UpdateManager->updatePlugin(\'Domdom.Cms\')\n#19 E:\\xampp\\htdocs\\alipo\\modules\\system\\console\\OctoberUp.php(45): System\\Classes\\UpdateManager->update()\n#20 [internal function]: System\\Console\\OctoberUp->handle()\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(29): call_user_func_array(Array, Array)\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(87): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(31): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(October\\Rain\\Foundation\\Application), Array, Object(Closure))\n#24 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(549): Illuminate\\Container\\BoundMethod::call(Object(October\\Rain\\Foundation\\Application), Array, Array, NULL)\n#25 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(183): Illuminate\\Container\\Container->call(Array)\n#26 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Command\\Command.php(252): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#27 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(170): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(946): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(248): Symfony\\Component\\Console\\Application->doRunCommand(Object(System\\Console\\OctoberUp), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 E:\\xampp\\htdocs\\alipo\\artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near \'json not null, `created_at` timestamp null, `updated_at` timestamp null) default\' at line 1 (SQL: create table `domdom_cms_clients` (`id` int unsigned not null auto_increment primary key, `clients` json not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8mb4 collate utf8mb4_unicode_ci engine = InnoDB) in E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php:664\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'create table `d...\', Array, Object(Closure))\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(459): Illuminate\\Database\\Connection->run(\'create table `d...\', Array, Object(Closure))\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Blueprint.php(86): Illuminate\\Database\\Connection->statement(\'create table `d...\')\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(252): Illuminate\\Database\\Schema\\Blueprint->build(Object(October\\Rain\\Database\\Connections\\MySqlConnection), Object(Illuminate\\Database\\Schema\\Grammars\\MySqlGrammar))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(165): Illuminate\\Database\\Schema\\Builder->build(Object(October\\Rain\\Database\\Schema\\Blueprint))\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\Schema\\Builder->create(\'domdom_cms_clie...\', Object(Closure))\n#6 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\updates\\create_clients_table.php(17): Illuminate\\Support\\Facades\\Facade::__callStatic(\'create\', Array)\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(35): Domdom\\Cms\\Updates\\CreateClientsTable->up()\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Concerns\\ManagesTransactions.php(29): October\\Rain\\Database\\Updater->October\\Rain\\Database\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(40): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#12 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(397): October\\Rain\\Database\\Updater->setUp(\'E:/xampp/htdocs...\')\n#13 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(146): System\\Classes\\VersionManager->applyDatabaseScript(\'Domdom.Cms\', \'1.0.6\', \'create_clients_...\')\n#14 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(99): System\\Classes\\VersionManager->applyPluginUpdate(\'Domdom.Cms\', \'1.0.6\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(519): System\\Classes\\VersionManager->updatePlugin(Object(Domdom\\Cms\\Plugin))\n#16 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(155): System\\Classes\\UpdateManager->updatePlugin(\'Domdom.Cms\')\n#17 E:\\xampp\\htdocs\\alipo\\modules\\system\\console\\OctoberUp.php(45): System\\Classes\\UpdateManager->update()\n#18 [internal function]: System\\Console\\OctoberUp->handle()\n#19 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(29): call_user_func_array(Array, Array)\n#20 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(87): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(31): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(October\\Rain\\Foundation\\Application), Array, Object(Closure))\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(549): Illuminate\\Container\\BoundMethod::call(Object(October\\Rain\\Foundation\\Application), Array, Array, NULL)\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(183): Illuminate\\Container\\Container->call(Array)\n#24 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Command\\Command.php(252): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#25 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(170): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#26 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(946): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#27 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(248): Symfony\\Component\\Console\\Application->doRunCommand(Object(System\\Console\\OctoberUp), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 E:\\xampp\\htdocs\\alipo\\artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 {main}', NULL, '2018-03-28 08:27:51', '2018-03-28 08:27:51');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(6, 'error', 'PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.domdom_cms_clients\' doesn\'t exist in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:77\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php(77): PDO->prepare(\'select count(*)...\', Array)\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(326): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'select count(*)...\')\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'select count(*)...\', Array)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select count(*)...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select count(*)...\', Array, Object(Closure))\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select count(*)...\', Array, true)\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1792): October\\Rain\\Database\\QueryBuilder->get()\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1765): Illuminate\\Database\\Query\\Builder->runPaginationCountQuery(Array)\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Builder.php(126): Illuminate\\Database\\Query\\Builder->getCountForPagination()\n#12 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(544): October\\Rain\\Database\\Builder->paginate(20, 1)\n#13 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(252): Backend\\Widgets\\Lists->getRecords()\n#14 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(240): Backend\\Widgets\\Lists->prepareVars()\n#15 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\listcontroller\\partials\\_container.htm(9): Backend\\Widgets\\Lists->render()\n#16 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#17 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\ControllerBehavior.php(143): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(98): Backend\\Classes\\ControllerBehavior->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#19 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(394): Backend\\Classes\\ControllerBehavior->makePartial(\'_container.htm\', Array)\n#20 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(381): Backend\\Behaviors\\ListController->listMakePartial(\'container\', Array)\n#21 [internal function]: Backend\\Behaviors\\ListController->listRender()\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(379): call_user_func_array(Array, Array)\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\Extendable.php(46): October\\Rain\\Extension\\Extendable->extendableCall(\'listRender\', Array)\n#24 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\controllers\\clients\\index.htm(2): October\\Rain\\Extension\\Extendable->__call(\'listRender\', Array)\n#25 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#26 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(110): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\')\n#27 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(365): Backend\\Classes\\Controller->makeView(\'index\')\n#28 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(247): Backend\\Classes\\Controller->execPageAction(\'index\', Array)\n#29 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\BackendController.php(112): Backend\\Classes\\Controller->run(\'index\', Array)\n#30 [internal function]: Backend\\Classes\\BackendController->run(\'domdom/cms/clie...\')\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#63 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#64 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#65 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#66 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.domdom_cms_clients\' doesn\'t exist in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:79\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(326): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'select count(*)...\')\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'select count(*)...\', Array)\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select count(*)...\', Array, Object(Closure))\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select count(*)...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select count(*)...\', Array, true)\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1792): October\\Rain\\Database\\QueryBuilder->get()\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1765): Illuminate\\Database\\Query\\Builder->runPaginationCountQuery(Array)\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Builder.php(126): Illuminate\\Database\\Query\\Builder->getCountForPagination()\n#11 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(544): October\\Rain\\Database\\Builder->paginate(20, 1)\n#12 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(252): Backend\\Widgets\\Lists->getRecords()\n#13 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(240): Backend\\Widgets\\Lists->prepareVars()\n#14 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\listcontroller\\partials\\_container.htm(9): Backend\\Widgets\\Lists->render()\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#16 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\ControllerBehavior.php(143): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(98): Backend\\Classes\\ControllerBehavior->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(394): Backend\\Classes\\ControllerBehavior->makePartial(\'_container.htm\', Array)\n#19 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(381): Backend\\Behaviors\\ListController->listMakePartial(\'container\', Array)\n#20 [internal function]: Backend\\Behaviors\\ListController->listRender()\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(379): call_user_func_array(Array, Array)\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\Extendable.php(46): October\\Rain\\Extension\\Extendable->extendableCall(\'listRender\', Array)\n#23 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\controllers\\clients\\index.htm(2): October\\Rain\\Extension\\Extendable->__call(\'listRender\', Array)\n#24 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#25 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(110): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\')\n#26 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(365): Backend\\Classes\\Controller->makeView(\'index\')\n#27 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(247): Backend\\Classes\\Controller->execPageAction(\'index\', Array)\n#28 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\BackendController.php(112): Backend\\Classes\\Controller->run(\'index\', Array)\n#29 [internal function]: Backend\\Classes\\BackendController->run(\'domdom/cms/clie...\')\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#63 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#64 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#65 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'alipo.domdom_cms_clients\' doesn\'t exist (SQL: select count(*) as aggregate from `domdom_cms_clients`) in E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php:664\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'select count(*)...\', Array, Object(Closure))\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(333): Illuminate\\Database\\Connection->run(\'select count(*)...\', Array, Object(Closure))\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1707): Illuminate\\Database\\Connection->select(\'select count(*)...\', Array, true)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1692): Illuminate\\Database\\Query\\Builder->runSelect()\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(122): Illuminate\\Database\\Query\\Builder->get(Array)\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\QueryBuilder.php(92): October\\Rain\\Database\\QueryBuilder->getDuplicateCached(Array)\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1792): October\\Rain\\Database\\QueryBuilder->get()\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(1765): Illuminate\\Database\\Query\\Builder->runPaginationCountQuery(Array)\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Builder.php(126): Illuminate\\Database\\Query\\Builder->getCountForPagination()\n#9 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(544): October\\Rain\\Database\\Builder->paginate(20, 1)\n#10 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(252): Backend\\Widgets\\Lists->getRecords()\n#11 E:\\xampp\\htdocs\\alipo\\modules\\backend\\widgets\\Lists.php(240): Backend\\Widgets\\Lists->prepareVars()\n#12 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\listcontroller\\partials\\_container.htm(9): Backend\\Widgets\\Lists->render()\n#13 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#14 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\ControllerBehavior.php(143): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(98): Backend\\Classes\\ControllerBehavior->makeFileContents(\'E:\\\\xampp\\\\htdocs...\', Array)\n#16 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(394): Backend\\Classes\\ControllerBehavior->makePartial(\'_container.htm\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\modules\\backend\\behaviors\\ListController.php(381): Backend\\Behaviors\\ListController->listMakePartial(\'container\', Array)\n#18 [internal function]: Backend\\Behaviors\\ListController->listRender()\n#19 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\ExtendableTrait.php(379): call_user_func_array(Array, Array)\n#20 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Extension\\Extendable.php(46): October\\Rain\\Extension\\Extendable->extendableCall(\'listRender\', Array)\n#21 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\controllers\\clients\\index.htm(2): October\\Rain\\Extension\\Extendable->__call(\'listRender\', Array)\n#22 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(244): include(\'E:\\\\xampp\\\\htdocs...\')\n#23 E:\\xampp\\htdocs\\alipo\\modules\\system\\traits\\ViewMaker.php(110): Backend\\Classes\\Controller->makeFileContents(\'E:\\\\xampp\\\\htdocs...\')\n#24 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(365): Backend\\Classes\\Controller->makeView(\'index\')\n#25 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\Controller.php(247): Backend\\Classes\\Controller->execPageAction(\'index\', Array)\n#26 E:\\xampp\\htdocs\\alipo\\modules\\backend\\classes\\BackendController.php(112): Backend\\Classes\\Controller->run(\'index\', Array)\n#27 [internal function]: Backend\\Classes\\BackendController->run(\'domdom/cms/clie...\')\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): call_user_func_array(Array, Array)\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction(\'run\', Array)\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(212): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(Backend\\Classes\\BackendController), \'run\')\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(169): Illuminate\\Routing\\Route->runController()\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Route->run()\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#34 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(41): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#35 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\View\\Middleware\\ShareErrorsFromSession.php(49): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#38 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\View\\Middleware\\ShareErrorsFromSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#39 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#40 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Session\\Middleware\\StartSession.php(63): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Session\\Middleware\\StartSession->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse.php(37): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#44 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\AddQueuedCookiesToResponse->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#45 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#46 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Cookie\\Middleware\\EncryptCookies.php(59): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#47 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Cookie\\Middleware\\EncryptCookies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#48 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#49 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#50 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(660): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#51 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(635): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#52 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(601): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#53 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Router\\CoreRouter.php(20): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#54 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(176): October\\Rain\\Router\\CoreRouter->dispatch(Object(Illuminate\\Http\\Request))\n#55 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(30): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#56 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(46): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#57 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(149): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#58 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php(53): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#59 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(102): Illuminate\\Routing\\Pipeline->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#60 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(151): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#61 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(116): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#62 E:\\xampp\\htdocs\\alipo\\index.php(43): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#63 {main}', NULL, '2018-03-28 08:30:06', '2018-03-28 08:30:06');
INSERT INTO `system_event_logs` (`id`, `level`, `message`, `details`, `created_at`, `updated_at`) VALUES
(7, 'error', 'PDOException: SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near \'json not null, `created_at` timestamp null, `updated_at` timestamp null) default\' at line 1 in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:77\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php(77): PDO->prepare(\'create table `d...\', Array)\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(452): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'create table `d...\')\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'create table `d...\', Array)\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'create table `d...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(459): Illuminate\\Database\\Connection->run(\'create table `d...\', Array, Object(Closure))\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Blueprint.php(86): Illuminate\\Database\\Connection->statement(\'create table `d...\')\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(252): Illuminate\\Database\\Schema\\Blueprint->build(Object(October\\Rain\\Database\\Connections\\MySqlConnection), Object(Illuminate\\Database\\Schema\\Grammars\\MySqlGrammar))\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(165): Illuminate\\Database\\Schema\\Builder->build(Object(October\\Rain\\Database\\Schema\\Blueprint))\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\Schema\\Builder->create(\'domdom_cms_clie...\', Object(Closure))\n#9 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\updates\\create_clients_table.php(17): Illuminate\\Support\\Facades\\Facade::__callStatic(\'create\', Array)\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(35): Domdom\\Cms\\Updates\\CreateClientsTable->up()\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Concerns\\ManagesTransactions.php(29): October\\Rain\\Database\\Updater->October\\Rain\\Database\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#12 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#13 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#14 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(40): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(397): October\\Rain\\Database\\Updater->setUp(\'E:/xampp/htdocs...\')\n#16 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(146): System\\Classes\\VersionManager->applyDatabaseScript(\'Domdom.Cms\', \'1.0.6\', \'create_clients_...\')\n#17 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(99): System\\Classes\\VersionManager->applyPluginUpdate(\'Domdom.Cms\', \'1.0.6\', Array)\n#18 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(519): System\\Classes\\VersionManager->updatePlugin(Object(Domdom\\Cms\\Plugin))\n#19 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(155): System\\Classes\\UpdateManager->updatePlugin(\'Domdom.Cms\')\n#20 E:\\xampp\\htdocs\\alipo\\modules\\system\\console\\OctoberUp.php(45): System\\Classes\\UpdateManager->update()\n#21 [internal function]: System\\Console\\OctoberUp->handle()\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(29): call_user_func_array(Array, Array)\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(87): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#24 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(31): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(October\\Rain\\Foundation\\Application), Array, Object(Closure))\n#25 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(549): Illuminate\\Container\\BoundMethod::call(Object(October\\Rain\\Foundation\\Application), Array, Array, NULL)\n#26 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(183): Illuminate\\Container\\Container->call(Array)\n#27 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Command\\Command.php(252): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(170): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(946): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(248): Symfony\\Component\\Console\\Application->doRunCommand(Object(System\\Console\\OctoberUp), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 E:\\xampp\\htdocs\\alipo\\artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#35 {main}\n\nNext Doctrine\\DBAL\\Driver\\PDOException: SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near \'json not null, `created_at` timestamp null, `updated_at` timestamp null) default\' at line 1 in E:\\xampp\\htdocs\\alipo\\vendor\\doctrine\\dbal\\lib\\Doctrine\\DBAL\\Driver\\PDOConnection.php:79\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(452): Doctrine\\DBAL\\Driver\\PDOConnection->prepare(\'create table `d...\')\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(657): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(\'create table `d...\', Array)\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'create table `d...\', Array, Object(Closure))\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(459): Illuminate\\Database\\Connection->run(\'create table `d...\', Array, Object(Closure))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Blueprint.php(86): Illuminate\\Database\\Connection->statement(\'create table `d...\')\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(252): Illuminate\\Database\\Schema\\Blueprint->build(Object(October\\Rain\\Database\\Connections\\MySqlConnection), Object(Illuminate\\Database\\Schema\\Grammars\\MySqlGrammar))\n#6 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(165): Illuminate\\Database\\Schema\\Builder->build(Object(October\\Rain\\Database\\Schema\\Blueprint))\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\Schema\\Builder->create(\'domdom_cms_clie...\', Object(Closure))\n#8 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\updates\\create_clients_table.php(17): Illuminate\\Support\\Facades\\Facade::__callStatic(\'create\', Array)\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(35): Domdom\\Cms\\Updates\\CreateClientsTable->up()\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Concerns\\ManagesTransactions.php(29): October\\Rain\\Database\\Updater->October\\Rain\\Database\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#12 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#13 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(40): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#14 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(397): October\\Rain\\Database\\Updater->setUp(\'E:/xampp/htdocs...\')\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(146): System\\Classes\\VersionManager->applyDatabaseScript(\'Domdom.Cms\', \'1.0.6\', \'create_clients_...\')\n#16 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(99): System\\Classes\\VersionManager->applyPluginUpdate(\'Domdom.Cms\', \'1.0.6\', Array)\n#17 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(519): System\\Classes\\VersionManager->updatePlugin(Object(Domdom\\Cms\\Plugin))\n#18 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(155): System\\Classes\\UpdateManager->updatePlugin(\'Domdom.Cms\')\n#19 E:\\xampp\\htdocs\\alipo\\modules\\system\\console\\OctoberUp.php(45): System\\Classes\\UpdateManager->update()\n#20 [internal function]: System\\Console\\OctoberUp->handle()\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(29): call_user_func_array(Array, Array)\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(87): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(31): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(October\\Rain\\Foundation\\Application), Array, Object(Closure))\n#24 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(549): Illuminate\\Container\\BoundMethod::call(Object(October\\Rain\\Foundation\\Application), Array, Array, NULL)\n#25 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(183): Illuminate\\Container\\Container->call(Array)\n#26 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Command\\Command.php(252): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#27 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(170): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(946): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(248): Symfony\\Component\\Console\\Application->doRunCommand(Object(System\\Console\\OctoberUp), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 E:\\xampp\\htdocs\\alipo\\artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 {main}\n\nNext Illuminate\\Database\\QueryException: SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near \'json not null, `created_at` timestamp null, `updated_at` timestamp null) default\' at line 1 (SQL: create table `domdom_cms_clients` (`id` int unsigned not null auto_increment primary key, `clients` json not null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8mb4 collate utf8mb4_unicode_ci engine = InnoDB) in E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php:664\nStack trace:\n#0 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(624): Illuminate\\Database\\Connection->runQueryCallback(\'create table `d...\', Array, Object(Closure))\n#1 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(459): Illuminate\\Database\\Connection->run(\'create table `d...\', Array, Object(Closure))\n#2 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Blueprint.php(86): Illuminate\\Database\\Connection->statement(\'create table `d...\')\n#3 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(252): Illuminate\\Database\\Schema\\Blueprint->build(Object(October\\Rain\\Database\\Connections\\MySqlConnection), Object(Illuminate\\Database\\Schema\\Grammars\\MySqlGrammar))\n#4 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Schema\\Builder.php(165): Illuminate\\Database\\Schema\\Builder->build(Object(October\\Rain\\Database\\Schema\\Blueprint))\n#5 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\Schema\\Builder->create(\'domdom_cms_clie...\', Object(Closure))\n#6 E:\\xampp\\htdocs\\alipo\\plugins\\domdom\\cms\\updates\\create_clients_table.php(17): Illuminate\\Support\\Facades\\Facade::__callStatic(\'create\', Array)\n#7 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(35): Domdom\\Cms\\Updates\\CreateClientsTable->up()\n#8 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Concerns\\ManagesTransactions.php(29): October\\Rain\\Database\\Updater->October\\Rain\\Database\\{closure}(Object(October\\Rain\\Database\\Connections\\MySqlConnection))\n#9 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\DatabaseManager.php(327): Illuminate\\Database\\Connection->transaction(Object(Closure))\n#10 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\Facades\\Facade.php(221): Illuminate\\Database\\DatabaseManager->__call(\'transaction\', Array)\n#11 E:\\xampp\\htdocs\\alipo\\vendor\\october\\rain\\src\\Database\\Updater.php(40): Illuminate\\Support\\Facades\\Facade::__callStatic(\'transaction\', Array)\n#12 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(397): October\\Rain\\Database\\Updater->setUp(\'E:/xampp/htdocs...\')\n#13 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(146): System\\Classes\\VersionManager->applyDatabaseScript(\'Domdom.Cms\', \'1.0.6\', \'create_clients_...\')\n#14 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\VersionManager.php(99): System\\Classes\\VersionManager->applyPluginUpdate(\'Domdom.Cms\', \'1.0.6\', Array)\n#15 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(519): System\\Classes\\VersionManager->updatePlugin(Object(Domdom\\Cms\\Plugin))\n#16 E:\\xampp\\htdocs\\alipo\\modules\\system\\classes\\UpdateManager.php(155): System\\Classes\\UpdateManager->updatePlugin(\'Domdom.Cms\')\n#17 E:\\xampp\\htdocs\\alipo\\modules\\system\\console\\OctoberUp.php(45): System\\Classes\\UpdateManager->update()\n#18 [internal function]: System\\Console\\OctoberUp->handle()\n#19 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(29): call_user_func_array(Array, Array)\n#20 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(87): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#21 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(31): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(October\\Rain\\Foundation\\Application), Array, Object(Closure))\n#22 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(549): Illuminate\\Container\\BoundMethod::call(Object(October\\Rain\\Foundation\\Application), Array, Array, NULL)\n#23 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(183): Illuminate\\Container\\Container->call(Array)\n#24 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Command\\Command.php(252): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#25 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(170): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#26 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(946): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#27 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(248): Symfony\\Component\\Console\\Application->doRunCommand(Object(System\\Console\\OctoberUp), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#28 E:\\xampp\\htdocs\\alipo\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#29 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(88): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#30 E:\\xampp\\htdocs\\alipo\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(121): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#31 E:\\xampp\\htdocs\\alipo\\artisan(35): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 {main}', NULL, '2018-03-28 08:30:28', '2018-03-28 08:30:28');

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `content_css` text COLLATE utf8mb4_unicode_ci,
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_mail_layouts`
--

INSERT INTO `system_mail_layouts` (`id`, `name`, `code`, `content_html`, `content_text`, `content_css`, `is_locked`, `created_at`, `updated_at`) VALUES
(1, 'Default layout', 'default', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-default\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n\n        <!-- Header -->\n        {% partial \'header\' body %}\n            {{ subject|raw }}\n        {% endpartial %}\n\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n\n        <!-- Footer -->\n        {% partial \'footer\' body %}\n            &copy; {{ \"now\"|date(\"Y\") }} {{ appName }}. All rights reserved.\n        {% endpartial %}\n\n    </table>\n\n</body>\n</html>', '{{ content|raw }}', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-03-21 09:21:06', '2018-03-21 09:21:06'),
(2, 'System layout', 'system', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\n<head>\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n</head>\n<body>\n    <style type=\"text/css\" media=\"screen\">\n        {{ brandCss|raw }}\n        {{ css|raw }}\n    </style>\n\n    <table class=\"wrapper layout-system\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n        <tr>\n            <td align=\"center\">\n                <table class=\"content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                    <!-- Email Body -->\n                    <tr>\n                        <td class=\"body\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">\n                            <table class=\"inner-body\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">\n                                <!-- Body content -->\n                                <tr>\n                                    <td class=\"content-cell\">\n                                        {{ content|raw }}\n\n                                        <!-- Subcopy -->\n                                        {% partial \'subcopy\' body %}\n                                            **This is an automatic message. Please do not reply to it.**\n                                        {% endpartial %}\n                                    </td>\n                                </tr>\n                            </table>\n                        </td>\n                    </tr>\n                </table>\n            </td>\n        </tr>\n    </table>\n\n</body>\n</html>', '{{ content|raw }}\n\n\n---\nThis is an automatic message. Please do not reply to it.', '@media only screen and (max-width: 600px) {\n    .inner-body {\n        width: 100% !important;\n    }\n\n    .footer {\n        width: 100% !important;\n    }\n}\n\n@media only screen and (max-width: 500px) {\n    .button {\n        width: 100% !important;\n    }\n}', 1, '2018-03-21 09:21:06', '2018-03-21 09:21:06');

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

CREATE TABLE `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_html` text COLLATE utf8mb4_unicode_ci,
  `content_text` text COLLATE utf8mb4_unicode_ci,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_parameters`
--

INSERT INTO `system_parameters` (`id`, `namespace`, `group`, `item`, `value`) VALUES
(1, 'system', 'update', 'count', '0'),
(2, 'system', 'core', 'hash', '\"eb0dccd7698b8d533b4758b812ea10ad\"'),
(3, 'system', 'core', 'build', '\"434\"'),
(4, 'system', 'update', 'retry', '1522337203'),
(5, 'cms', 'theme', 'active', '\"alipo\"');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_history`
--

INSERT INTO `system_plugin_history` (`id`, `code`, `type`, `version`, `detail`, `created_at`) VALUES
(1, 'October.Demo', 'comment', '1.0.1', 'First version of Demo', '2018-03-21 09:21:06'),
(2, 'BenFreke.MenuManager', 'script', '1.0.1', 'create_menus_table.php', '2018-03-28 08:26:35'),
(3, 'BenFreke.MenuManager', 'comment', '1.0.1', 'First version of MenuManager', '2018-03-28 08:26:35'),
(4, 'BenFreke.MenuManager', 'comment', '1.0.2', 'Added active state to menu; Added ability to select active menu item; Added controllable depth to component', '2018-03-28 08:26:35'),
(5, 'BenFreke.MenuManager', 'comment', '1.0.3', 'Made it possible for menu items to not link anywhere; Put a check in so the active node must be a child of the parentNode', '2018-03-28 08:26:36'),
(6, 'BenFreke.MenuManager', 'comment', '1.0.4', 'Fixed bug where re-ordering stopped working', '2018-03-28 08:26:36'),
(7, 'BenFreke.MenuManager', 'comment', '1.0.5', 'Moved link creation code into the model in preparation for external links; Brought list item class creation into the model; Fixed typo with default menu class', '2018-03-28 08:26:36'),
(8, 'BenFreke.MenuManager', 'comment', '1.0.6', 'Removed NestedSetModel, thanks @daftspunk', '2018-03-28 08:26:36'),
(9, 'BenFreke.MenuManager', 'script', '1.1.0', 'add_is_external_field.php', '2018-03-28 08:26:36'),
(10, 'BenFreke.MenuManager', 'script', '1.1.0', 'add_link_target_field.php', '2018-03-28 08:26:37'),
(11, 'BenFreke.MenuManager', 'comment', '1.1.0', 'Added ability to link to external sites. Thanks @adisos', '2018-03-28 08:26:37'),
(12, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_1.php', '2018-03-28 08:26:37'),
(13, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_2.php', '2018-03-28 08:26:38'),
(14, 'BenFreke.MenuManager', 'script', '1.1.1', 'add_enabled_parameters_query_fields_3.php', '2018-03-28 08:26:38'),
(15, 'BenFreke.MenuManager', 'comment', '1.1.1', 'Added ability to enable/disable individual menu links; Added ability for url parameters & query string; Fixed issue of \"getLinkHref()\" pulling through full page url with parameters rather than the ACTUAL page url', '2018-03-28 08:26:38'),
(16, 'BenFreke.MenuManager', 'comment', '1.1.2', 'Reformatted code for better maintainability and better practises', '2018-03-28 08:26:39'),
(17, 'BenFreke.MenuManager', 'comment', '1.1.3', 'Fixed bug that prevented multiple components displaying', '2018-03-28 08:26:39'),
(18, 'BenFreke.MenuManager', 'comment', '1.2.0', 'Fixed validation and fields bug; Added list classes', '2018-03-28 08:26:39'),
(19, 'BenFreke.MenuManager', 'comment', '1.3.0', 'Added translation ability thanks to @alxy', '2018-03-28 08:26:39'),
(20, 'BenFreke.MenuManager', 'comment', '1.3.1', 'JSON validation of parameters added; Correct active menu now being set based on parameters; PR sent by @whsol, thanks!', '2018-03-28 08:26:39'),
(21, 'BenFreke.MenuManager', 'comment', '1.3.2', 'Fix for param check that is breaking active node lookup. Thanks @alxy', '2018-03-28 08:26:39'),
(22, 'BenFreke.MenuManager', 'comment', '1.3.3', 'Fix for JSON comment having single quotes. Thanks @adisos', '2018-03-28 08:26:39'),
(23, 'BenFreke.MenuManager', 'comment', '1.3.4', 'Fix for JSON validation not firing', '2018-03-28 08:26:39'),
(24, 'BenFreke.MenuManager', 'script', '1.4.0', 'fix_menu_table.php', '2018-03-28 08:26:40'),
(25, 'BenFreke.MenuManager', 'comment', '1.4.0', 'Fix for POST operations. PR by @matissjanis, TR translations (@mahony0) and permission registration (@nnmer)', '2018-03-28 08:26:40'),
(26, 'BenFreke.MenuManager', 'comment', '1.4.1', 'Fixed bug caused by deleting needed method of Menu class. Thanks @MatissJA', '2018-03-28 08:26:40'),
(27, 'BenFreke.MenuManager', 'comment', '1.4.2', 'Fixed bug with URLs not saving correctly', '2018-03-28 08:26:40'),
(28, 'BenFreke.MenuManager', 'comment', '1.4.3', 'Fixed bug where getBaseFileName method was moved to a different object', '2018-03-28 08:26:40'),
(29, 'BenFreke.MenuManager', 'comment', '1.4.4', 'Fixed bug with incorrect labels. Thanks @ribsousa', '2018-03-28 08:26:40'),
(30, 'BenFreke.MenuManager', 'comment', '1.4.5', 'Fixed bug where getBaseFileName method was moved to a different object', '2018-03-28 08:26:40'),
(31, 'BenFreke.MenuManager', 'comment', '1.4.6', 'Merged PRs that fix bug with plugin not working with stable release', '2018-03-28 08:26:40'),
(32, 'BenFreke.MenuManager', 'comment', '1.4.7', 'Merged PR to fix syntax errors with fresh install of 1.4.6. Thanks @devlifeX', '2018-03-28 08:26:40'),
(33, 'BenFreke.MenuManager', 'comment', '1.4.8', 'Merged PR to fix re-order errors. Thanks @CptMeatball', '2018-03-28 08:26:41'),
(34, 'BenFreke.MenuManager', 'comment', '1.5.0', 'Fixed bugs preventing postgres and sqlite support', '2018-03-28 08:26:41'),
(35, 'BenFreke.MenuManager', 'comment', '1.5.1', 'Added homepage to plugin information. Thanks @gergo85', '2018-03-28 08:26:41'),
(36, 'BenFreke.MenuManager', 'script', '1.5.2', 'Added code of conduct', '2018-03-28 08:26:41'),
(37, 'BenFreke.MenuManager', 'comment', '1.5.2', 'Added French translation. Thanks @Glitchbone', '2018-03-28 08:26:41'),
(38, 'Domdom.Cms', 'script', '1.0.5', 'create_homes_table.php', '2018-03-28 08:26:41'),
(39, 'Domdom.Cms', 'script', '1.0.5', 'create_abouts_table.php', '2018-03-28 08:26:42'),
(40, 'Domdom.Cms', 'script', '1.0.6', 'create_homes_table.php', '2018-03-28 08:27:50'),
(41, 'Domdom.Cms', 'script', '1.0.6', 'create_abouts_table.php', '2018-03-28 08:27:50'),
(42, 'Domdom.Cms', 'script', '1.0.6', 'create_clients_table.php', '2018-03-28 08:32:45'),
(43, 'Domdom.Cms', 'script', '1.0.6', 'create_contacts_table.php', '2018-03-28 08:32:45'),
(44, 'Domdom.Cms', 'script', '1.0.6', 'create_ganeral_options_table.php', '2018-03-28 08:32:45'),
(45, 'Domdom.Cms', 'script', '1.0.6', 'create_reels_table.php', '2018-03-28 08:32:46'),
(46, 'Domdom.Cms', 'script', '1.0.6', 'create_works_table.php', '2018-03-28 08:32:46'),
(47, 'Domdom.Cms', 'script', '1.0.6', 'create_category_works_table.php', '2018-03-28 08:32:46'),
(48, 'Domdom.Cms', 'comment', '1.0.6', 'First version of cms', '2018-03-28 08:32:46'),
(49, 'LaminSanneh.FlexiContact', 'comment', '1.0.0', 'First Version with basic functionality', '2018-03-28 08:32:46'),
(50, 'LaminSanneh.FlexiContact', 'comment', '1.0.1', 'Fixed email subject to send actual subject set in backend', '2018-03-28 08:32:47'),
(51, 'LaminSanneh.FlexiContact', 'comment', '1.0.2', 'Added validation to contact form fields', '2018-03-28 08:32:47'),
(52, 'LaminSanneh.FlexiContact', 'comment', '1.0.3', 'Changed body input field type from text to textarea', '2018-03-28 08:32:47'),
(53, 'LaminSanneh.FlexiContact', 'comment', '1.0.4', 'Updated default component markup to use more appropriate looking twitter bootstrap classes', '2018-03-28 08:32:47'),
(54, 'LaminSanneh.FlexiContact', 'comment', '1.0.5', 'Corrected spelling for Marketing on the backend settings', '2018-03-28 08:32:47'),
(55, 'LaminSanneh.FlexiContact', 'script', '1.0.6', 'Add proper validation message outputting', '2018-03-28 08:32:47'),
(56, 'LaminSanneh.FlexiContact', 'script', '1.0.6', 'Added option to include or exclude main script file', '2018-03-28 08:32:47'),
(57, 'LaminSanneh.FlexiContact', 'comment', '1.0.6', 'Added ability to include bootstrap or not on component config', '2018-03-28 08:32:47'),
(58, 'LaminSanneh.FlexiContact', 'script', '1.0.7', 'Updated readme file', '2018-03-28 08:32:47'),
(59, 'LaminSanneh.FlexiContact', 'comment', '1.0.7', 'Updated contact component default markup file', '2018-03-28 08:32:47'),
(60, 'LaminSanneh.FlexiContact', 'comment', '1.1.0', 'Mail template is now registered properly', '2018-03-28 08:32:47'),
(61, 'LaminSanneh.FlexiContact', 'comment', '1.2.0', 'Add Proper validation that can be localized', '2018-03-28 08:32:47'),
(62, 'LaminSanneh.FlexiContact', 'comment', '1.2.1', 'Added permissions to the settings page. PR by @matissjanis', '2018-03-28 08:32:47'),
(63, 'LaminSanneh.FlexiContact', 'comment', '1.2.2', 'Added polish language features', '2018-03-28 08:32:47'),
(64, 'LaminSanneh.FlexiContact', 'comment', '1.2.3', 'Modified mail templatedefault text', '2018-03-28 08:32:48'),
(65, 'LaminSanneh.FlexiContact', 'comment', '1.3.0', '!!! Added captcha feature, which requires valid google captcha credentials to work', '2018-03-28 08:32:48'),
(66, 'LaminSanneh.FlexiContact', 'comment', '1.3.1', 'Set replyTo instead of from-header when sending', '2018-03-28 08:32:48'),
(67, 'LaminSanneh.FlexiContact', 'comment', '1.3.2', 'Added german translation language file', '2018-03-28 08:32:48'),
(68, 'LaminSanneh.FlexiContact', 'comment', '1.3.3', 'Added option to allow user to enable or disable captcha in contact form', '2018-03-28 08:32:48'),
(69, 'LaminSanneh.FlexiContact', 'comment', '1.3.4', 'Made sure captcha enabling or disabling doesnt produce bug', '2018-03-28 08:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `is_frozen` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_plugin_versions`
--

INSERT INTO `system_plugin_versions` (`id`, `code`, `version`, `created_at`, `is_disabled`, `is_frozen`) VALUES
(1, 'October.Demo', '1.0.1', '2018-03-21 09:21:06', 0, 0),
(2, 'BenFreke.MenuManager', '1.5.2', '2018-03-28 08:26:41', 0, 0),
(3, 'Domdom.Cms', '1.0.6', '2018-03-28 08:32:46', 0, 0),
(4, 'LaminSanneh.FlexiContact', '1.3.4', '2018-03-28 08:32:48', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8mb4_unicode_ci,
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`),
  ADD KEY `admin_role_index` (`role_id`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_unique` (`name`),
  ADD KEY `role_code_index` (`code`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `benfreke_menumanager_menus`
--
ALTER TABLE `benfreke_menumanager_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `benfreke_menumanager_menus_parent_id_index` (`parent_id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_logs_type_index` (`type`),
  ADD KEY `cms_theme_logs_theme_index` (`theme`),
  ADD KEY `cms_theme_logs_user_id_index` (`user_id`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`),
  ADD KEY `deferred_bindings_session_key_index` (`session_key`);

--
-- Indexes for table `domdom_cms_abouts`
--
ALTER TABLE `domdom_cms_abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domdom_cms_category_works`
--
ALTER TABLE `domdom_cms_category_works`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domdom_cms_clients`
--
ALTER TABLE `domdom_cms_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domdom_cms_contacts`
--
ALTER TABLE `domdom_cms_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domdom_cms_ganeral_options`
--
ALTER TABLE `domdom_cms_ganeral_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domdom_cms_homes`
--
ALTER TABLE `domdom_cms_homes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domdom_cms_reels`
--
ALTER TABLE `domdom_cms_reels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domdom_cms_works`
--
ALTER TABLE `domdom_cms_works`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `benfreke_menumanager_menus`
--
ALTER TABLE `benfreke_menumanager_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domdom_cms_abouts`
--
ALTER TABLE `domdom_cms_abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domdom_cms_category_works`
--
ALTER TABLE `domdom_cms_category_works`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domdom_cms_clients`
--
ALTER TABLE `domdom_cms_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domdom_cms_contacts`
--
ALTER TABLE `domdom_cms_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domdom_cms_ganeral_options`
--
ALTER TABLE `domdom_cms_ganeral_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domdom_cms_homes`
--
ALTER TABLE `domdom_cms_homes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domdom_cms_reels`
--
ALTER TABLE `domdom_cms_reels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `domdom_cms_works`
--
ALTER TABLE `domdom_cms_works`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
