<?php namespace Domdom\Cms\Models;

use Model;

/**
 * Home Model
 */
class Home extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_homes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['video_top'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'feature_image' => 'System\Models\File'
    ];
    public $attachMany = [];
}
