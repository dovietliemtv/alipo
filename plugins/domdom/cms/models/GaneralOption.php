<?php namespace Domdom\Cms\Models;

use Model;

/**
 * GaneralOption Model
 */
class GaneralOption extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_ganeral_options';
    public $rules = [
        'logo_normal'   =>  'required',
        'logo_redmenu'   =>  'required',
    ];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];


    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['social_network'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'logo_normal'   =>  'System\Models\File',
        'logo_redmenu'   =>  'System\Models\File'
    ];
    public $attachMany = [];

    public function getNameSocialOptions() {
        return [
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'vimeo' => 'Vimeo',
        ];
    }
}
