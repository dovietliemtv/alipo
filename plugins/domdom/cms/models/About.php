<?php namespace Domdom\Cms\Models;

use Model;

/**
 * About Model
 */
class About extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_abouts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'feature_image' =>  'System\Models\File'
    ];
    public $attachMany = [];
}
