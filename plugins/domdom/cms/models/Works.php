<?php namespace Domdom\Cms\Models;

use Model;

/**
 * Works Model
 */
class Works extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_works';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['project_info'];
    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'category' => ['DomDom\Cms\Models\CategoryWork', 'key' => 'category_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'feature_image' =>  'System\Models\File',
        'gif_image' =>  'System\Models\File'
    ];
    public $attachMany = [
        'gallery_image' =>  'System\Models\File'
    ];
}
