<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHomesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_homes')){
            Schema::create('domdom_cms_homes', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('heading');
                $table->text('video_top');
                $table->timestamps();
            });
        }
    }
    public function down()
    {
        Schema::dropIfExists('domdom_cms_homes');
    }
}
