<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateClientsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_clients')) {
            Schema::create('domdom_cms_clients', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('clients');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_clients');
    }
}
