<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategoryWorksTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_category_works')) {
            Schema::create('domdom_cms_category_works', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('category_work_name');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_category_works');
    }
}
