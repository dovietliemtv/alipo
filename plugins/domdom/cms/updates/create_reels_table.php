<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateReelsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_reels')) {
            Schema::create('domdom_cms_reels', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('vimeo_id');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_reels');
    }
}
