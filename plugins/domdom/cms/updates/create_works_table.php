<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWorksTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_works')) {
            Schema::create('domdom_cms_works', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('title');
                $table->text('slug');
                $table->text('description');
                $table->text('category_id');
                $table->text('vimeo_id');
                $table->text('introduce_title');
                $table->text('introduce_description');
                $table->text('project_info');
                $table->timestamps();
            });
        }
        Schema::table('domdom_cms_works', function($table)
        {
            $table->text('description')->after('slug');

        });
    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_works');
    }
}
