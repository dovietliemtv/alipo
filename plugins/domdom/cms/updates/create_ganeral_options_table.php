<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGaneralOptionsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_ganeral_options')) {
            Schema::create('domdom_cms_ganeral_options', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('social_network');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_ganeral_options');
    }
}
