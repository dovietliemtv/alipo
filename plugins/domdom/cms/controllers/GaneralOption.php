<?php namespace Domdom\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Ganeral Option Back-end Controller
 */
class GaneralOption extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Domdom.Cms', 'cms', 'ganeraloption');
    }
}
