<?php namespace Domdom\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
/**
 * Works Back-end Controller
 */
class Works extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Domdom.Cms', 'cms', 'works');
    }
    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'feature_image' ) {
            return '<img src="' . $record->feature_image->path . '" width="75"/>';
        }
    }
}
