<?php namespace Domdom\Cms;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * cms Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'cms',
            'description' => 'No description provided yet...',
            'author'      => 'domdom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Domdom.Cms', 'cms', 'plugins/domdom/cms/partials/sidebar');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        //return []; // Remove this line to activate

        return [
            'Domdom\Cms\Components\CpHome' => 'cpHome',
            'Domdom\Cms\Components\CpReel' => 'cpReel',
            'Domdom\Cms\Components\CpWorks' => 'cpWorks',
            'Domdom\Cms\Components\CpWorkDetail' => 'cpWorkDetail',
            'Domdom\Cms\Components\CpClients' => 'cpClients',
            'Domdom\Cms\Components\CpAbout' => 'cpAbout',
            'Domdom\Cms\Components\CpContact' => 'cpContact',
            'Domdom\Cms\Components\CpGaneralOption' => 'cpGaneralOption',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.cms.some_permission' => [
                'tab' => 'cms',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        //return []; // Remove this line to activate

        return [
            'cms' => [
                'label'       => 'CMS Custom',
                'url'         => Backend::url('domdom/cms/home/update/1'),
                'icon'        => 'icon-leaf',
                'permissions' => ['domdom.cms.*'],
                'order'       => 500,
                'sideMenu' => [
                    'homepage' => [
                        'label' => 'Homepage',
                        'url'   => Backend::url('domdom/cms/home/update/1'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'pages'
                    ],
                    'aboutpage' => [
                        'label' => 'About page',
                        'url'   => Backend::url('domdom/cms/about/update/1'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'pages'
                    ],
                    'reel' => [
                        'label' => 'Reel page',
                        'url'   => Backend::url('domdom/cms/reel/update/1'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'pages'
                    ],
                    'clients' => [
                        'label' => 'Clients page',
                        'url'   => Backend::url('domdom/cms/clients/update/1'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'pages'
                    ],
                    'contact' => [
                        'label' => 'Contact page',
                        'url'   => Backend::url('domdom/cms/contact/update/1'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'pages'
                    ],

                    'workspage' => [
                        'label' => 'Works page',
                        'url'   => Backend::url('domdom/cms/works/'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'Works'
                    ],
                    'categorywork' => [
                        'label' => 'Category work',
                        'url'   => Backend::url('domdom/cms/categorywork/'),
                        'icon'        => 'icon-plus-circle',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'Works'
                    ],
                    'generaloption' =>[
                        'label' => 'General Options',
                        'url'   => Backend::url('domdom/cms/ganeraloption/update/1'),
                        'icon'        => 'icon-glide-g',
                        'permissions' => ['domdom.cms.*'],
                        'order'       => 500,
                        'group'       => 'General Options'
                    ],

                ]
            ],
        ];
    }
}
