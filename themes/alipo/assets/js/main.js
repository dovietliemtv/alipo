
'use trick';
(function( $ ){
    $(document).ready(function () {
        init();

        // set width height vimeo
        var screemW = window.innerWidth;
        // setVimeoSize(screemW);
    });
    $(window).on('resize', function () {
        var screemW = window.innerWidth;
        // setVimeoSize(screemW);
    });

    init = function () {

        var body = $('body');
        $('.js-open-menu').on('click', function () {
            body.toggleClass('is-opened');
        });


        // fullpage home
        $('#fullpage').fullpage({
            scrollOverflow: false,
            afterRender: function(){
                $('#video')[0].play();
            }
        });
        //slider work detail
        $('.dd-b-work-detail-slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            prevArrow: '<div class="arrow pre"><span></span></div>',
            nextArrow: '<div class="arrow next"><span></span></div>',
        });

        // slider work detail arrow click
        $('.arrow-slider-pre').on('click', function (e) {
            e.preventDefault();
            $('.pre').click();
        });
        $('.arrow-slider-next').on('click', function (e) {
            e.preventDefault();
            $('.next').click();
        });

        // popup work detail
        $('.dd-b-work-detail-slider .dd-b-slide').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            zoom: {
                enabled: true,
                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out' // CSS transition
            }
        });
        
    }; // end init
    
    
    function setVimeoSize(screemW) {
        var videoH = screemW*900/1600;
        $('.dd-vimeo iframe').css({
            'width': screemW,
            'height': videoH + 'px'
        })
    }
})( jQuery );
